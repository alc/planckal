# planckal

A very optimized layout for french and english on the Planck keyboard inspired by bépo and éwopy with ideas from Colemak layouts. The goal being to be as efficient in english than in french, it emphasizes hand altenating more than finger rolling.

It contains a QMK firmware created with the oryx (ergodox) tool but should work with the olkb hardware too (not tested).
The keymap and the spreadsheet usep for effort calculation can also be used to adapt the layout to any software.

The firmware can also be find on oryx website : https://configure.zsa.io/planck-ez/layouts/jXoQJ/latest/0